package com.lab.bonus3

import android.Manifest.permission.CAMERA
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import com.google.firebase.storage.FirebaseStorage
import java.util.UUID.randomUUID
import com.google.firebase.storage.StorageReference
import java.util.*
import com.google.firebase.storage.UploadTask
import com.google.firebase.storage.OnProgressListener
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.net.toUri
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener




//Código para tomar fotos adaptado de http://www.kotlincodes.com/kotlin/camera-intent-with-kotlin-android/
//Código para subir la foto adaptado de https://code.tutsplus.com/tutorials/image-upload-to-firebase-in-android-application--cms-29934
class MainActivity : AppCompatActivity() {

    val CAMERA_PERMISSION_REQUEST_CODE = 1
    val CAMERA_IMG_TAKEN = 1
    var tempPhotoPath: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(!checkPersmission()){
            requestPermission()
        }

        val btn: Button = findViewById(R.id.upBtn)
        btn.isClickable = false
        btn.setAlpha(.5f);
    }

    private fun checkPersmission(): Boolean {
        return (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
            android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(CAMERA), CAMERA_PERMISSION_REQUEST_CODE)
    }

    fun takePicture( v: View ) {
        val intent: Intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)


        var tempFile: File =  File.createTempFile(
            "JPEG_1", /* prefix */
            ".jpg", /* suffix */
            getExternalFilesDir(Environment.DIRECTORY_PICTURES) /* directory */
        ).apply{
            tempPhotoPath = absolutePath
        }

        val uri: Uri = FileProvider.getUriForFile(
            this,
            "com.example.android.fileprovider",
            tempFile
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT,uri)

        startActivityForResult(intent, CAMERA_IMG_TAKEN)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CAMERA_IMG_TAKEN && resultCode == Activity.RESULT_OK) {

            var bitmap: Bitmap = BitmapFactory.decodeFile(tempPhotoPath)
            imageView.setImageBitmap(bitmap)

            val btn: Button = findViewById(R.id.upBtn)
            btn.isClickable = true
            btn.setAlpha(1f);

        }
    }

    //Código para subir la foto adaptado de https://code.tutsplus.com/tutorials/image-upload-to-firebase-in-android-application--cms-29934
    fun uploadImg(v: View){
        val storage = FirebaseStorage.getInstance()
        val storageReference = storage.getReference();

        val progressDialog: ProgressDialog = ProgressDialog(this);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();

        val ref = storageReference.child("images/" + UUID.randomUUID().toString())

        ref.putFile(File(tempPhotoPath).toUri())
            .addOnSuccessListener {
                progressDialog.dismiss()
                Toast.makeText(this@MainActivity, "Uploaded", Toast.LENGTH_SHORT).show()
                imageView.setImageResource(0)
            }
            .addOnFailureListener { e ->
                progressDialog.dismiss()
                Toast.makeText(this@MainActivity, "Failed " + e.message, Toast.LENGTH_SHORT).show()
            }
            .addOnProgressListener { taskSnapshot ->
                val progress = 100.0 * taskSnapshot.bytesTransferred / taskSnapshot
                    .totalByteCount
                progressDialog.setMessage("Uploaded " + progress.toInt() + "%")
            }
    }

}
